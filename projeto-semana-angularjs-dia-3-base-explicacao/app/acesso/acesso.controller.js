(function () {
    "use strict";

    // CONTROLLER
    angular.module('listaComprasApp')
        .controller('AcessoController', acessoController);

        acessoController.$inject = [];

    function acessoController() {
        var vm = this;
        /* ***************    INIT VARIÁVEIS    *********************************** */


        /* ***************    FUNÇÕES EXECUTADAS NA VIEW (HMTL)    **************** */


        /* ***************    FUNÇÕES INSTERNAS    ******************************** */

    }

})();