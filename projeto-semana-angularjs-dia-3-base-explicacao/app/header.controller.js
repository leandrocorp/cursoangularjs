(function () {
    "use strict";

    // CONTROLLER
    angular.module('listaComprasApp')
        .controller('HeaderController', headerController);

        headerController.$inject = ['$location', 'ListaComprasService'];

    function headerController($location, service) {
        var vm = this;
        /* ***************    INIT VARIÁVEIS    *********************************** */


        /* ***************    FUNÇÕES EXECUTADAS NA VIEW (HMTL)    **************** */

        vm.go = go;
        vm.service = service.exemplo;

        function go(_path){
            service.exemplo();
            $location.path(_path);

        }   
        
        /* ***************    FUNÇÕES INSTERNAS    ******************************** */

    }

})();