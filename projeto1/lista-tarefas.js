(function (){

    //modulo
    angular.module('listaTarefasApp', []);

    //controller
    angular.module('listaTarefasApp')
    .controller("listaTarefasController", listaTarefasController);
      
        listaTarefasController.$inject = ['$scope'];
       
        function listaTarefasController ($scope){
            var vm = this; // referenciando o contexto da funcao

            vm.tarefas = [
                {text: 'estudar angularJS', feito: true },
                {text: 'fazer uma aplicacao em angularJS', feito:false}
                ];

                vm.restam =restam; // sempre declarar de forma isolada 
                vm.addTarefa = addTarefa;
                vm.arquivar = arquivar;
                vm.calculadora = calculadora;

                function restam(params){
                    var count = 0;
                    angular.forEach(vm.tarefas, function (trf){
                        if(!trf.feito) count++; // se ela foi feita a tarefa
                    });
                    return count;
                }

                function addTarefa(params){
                    vm.tarefas.push({text: vm.tarefaText, feito: false});
                    vm.tarefaText = '';
                }

                function arquivar(){
                 /* var oldTarefas = vm.tarefas;
                    vm.tarefas = [];
                    angular.forEach(oldTarefas, function (trf){
                        if(!trf.feito){
                          console.log(angular.copy(vm.tarefas)); 
                            vm.tarefas.push(trf);
                        } 
                    });*/
                    vm.tarefas = vm.tarefas.filter(function (trf){ return !trf.feito});
                    
                }

                function calculadora(){
                    
                }



            }
})();